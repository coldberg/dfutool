#include "dfuse.h"
#include <map>
#include <vector>
#include <regex>
#include <iostream>


struct tool : dfu::dfuse{
	
	tool ();

	int help (int first, int argc, char **argv);
	int add_dfuse (int first, int argc, char **argv);
	int add_srecord (int first, int argc, char **argv);
	int add_multibin (int first, int argc, char **argv);
	int write_dfuse (int first, int argc, char **argv);
	int write_srecord (int first, int argc, char **argv);
	int write_multibin (int first, int argc, char **argv);
	int vendor_id (int first, int argc, char **argv);
	int product_id (int first, int argc, char **argv);
	int version (int first, int argc, char **argv);

	int main (int argc, char ** argv);

	std::vector<std::string> split(std::string const &input, std::string const &regex) {	    
		std::regex rx (regex);
		std::sregex_token_iterator
			first (input.begin(), input.end(), rx, -1),
			last;
		return std::vector<std::string> (first, last);
	}	

private:
	typedef decltype (&tool::help) command_exec_t;
	std::map<std::string, command_exec_t> command_list;
	int iSplitSize;
};

tool::tool () : 
	dfu::dfuse (0x0000, 0x0483, 0x0000),
	command_list ({
		{"--help",				&tool::help},
		{"--add-dfuse",			&tool::add_dfuse},
		{"--add-srecord",		&tool::add_srecord},
		{"--add-multibin",		&tool::add_multibin},
		{"--write-dfuse",		&tool::write_dfuse},
		{"--write-srecord",		&tool::write_srecord},
		{"--write-multibin",	&tool::write_multibin},
		{"--vendor-id",			&tool::vendor_id},
		{"--product-id",		&tool::product_id},
		{"--version",			&tool::version}
	}),
	iSplitSize (30)
{
		
}

int tool::help (int first, int argc, char ** argv) {
	std::cout 
	<< "dfutool --add-command args --add-command args ... --write-command args\n"
		<< "\t --help                                                                             - display this screen\n"
		<< "\t --add-dfuse filename                                                               - load .dfu file\n"
		<< "\t --add-multibin 0xADDRESS:filename [, address:filename] [,target-id] [,target-name] - load multiple .bin files\n"
		<< "\t --add-srecord filename [,target-id] [,target-name]                                 - load .s19 file\n"
		<< "\t --write-dfuse filename                                                             - write .dfu file\n"
		<< "\t --write-multibin filename                                                          - write multi-bin files\n"
		<< "\t --write-srecord filename                                                           - write multi-srecord files\n"
		<< "\t --vendor-id 0xVENDORID                                                             - set vendor id for output .dfu\n"
		<< "\t --product-id 0xPRODUCTID                                                           - set product id for output .dfu\n"
		<< "\t --version 0xVERSION                                                                - set version for output .dfu\n";

	return 0;
}
int tool::add_dfuse (int first, int argc, char ** argv) {
	if (first >= argc)
		throw std::runtime_error ("Too few arguments to --add-dfuse");
	dfuse::add_dfuse (argv [first]);
	return 1;
}
int tool::write_dfuse (int first, int argc, char ** argv) {
	if (first >= argc)
		throw std::runtime_error ("Too few arguments to --write-dfuse");
	dfuse::write_dfuse (argv [first]);
	return 1;
}

int tool::add_multibin (int first, int argc, char ** argv) {
	if (first >= argc)
		throw std::runtime_error ("Too few arguments to --add-multibin");
	auto tl = split (argv [first], ",");

	std::string pTargeName = "ST...";
	dfu::byte_t iTargetId = 0;
	dfu::binary_map_t pBinaryMap;

	size_t i;
	for (i = 0; i < tl.size (); ++i) {
		auto fd = tl [i];
		std::smatch result;
		if (!std::regex_match (fd, result, std::regex ("(0x[0-9A-Fa-f]+):([^:]+)"))) {
			break;
		}		
		std::string address = result [1].str ();
		dfu::uoff_t _address = std::stoul (address, NULL, 0);
		std::string fileName = result [2].str ();
		pBinaryMap.push_back ({_address, fileName});
	}

	if (i < tl.size ()) {
		iTargetId = std::stoul (tl [i]) & 0xff;
		++i;
	}

	if (i < tl.size ()) {
		pTargeName = tl [i];
		++i;
	}
	if (pBinaryMap.size () < 1)
		throw std::runtime_error ("No address:binary pairs specified in --add-multibin");
	dfuse::add_multibin (pBinaryMap, iTargetId, pTargeName);
	return 1;
}
int tool::add_srecord (int first, int argc, char ** argv) {
	if (first >= argc)
		throw std::runtime_error ("Too few arguments to --add-srecord");
	auto tl = split (argv [first], ",");
	
	std::string pTargeName = "ST...";
	dfu::byte_t iTargetId = 0;
	std::string fileName = tl [0];

	if (tl.size () > 1) {
		iTargetId = std::stoul (tl [1]) & 0xff;
	}

	if (tl.size () > 2) {
		pTargeName = tl [2];
	}

	dfuse::add_srecord (fileName, iTargetId, pTargeName);
	return 1;
}

int tool::write_srecord (int first, int argc, char ** argv) {
	if (first >= argc)
		throw std::runtime_error ("Too few arguments to --write-srecord");
	dfuse::write_srecord (argv [first], iSplitSize);
	return 1;
}

int tool::write_multibin (int first, int argc, char ** argv) {
	if (first >= argc)
		throw std::runtime_error ("Too few arguments to --write-multibin");
	dfuse::write_multibin (argv [first]);
	return 1;
}

int tool::vendor_id (int first, int argc, char ** argv) {
	if (first >= argc)
		throw std::runtime_error ("Too few arguments to --vendor-id");
	dfuse::vendor_id (std::stoul (argv [first], 0, 16) & 0xffff);
	return 1;
}
int tool::product_id (int first, int argc, char ** argv) {
	if (first >= argc)
		throw std::runtime_error ("Too few arguments to --product-id");
	dfuse::product_id (std::stoul (argv [first], 0, 16) & 0xffff);
	return 1;
}
int tool::version (int first, int argc, char ** argv) {
	if (first >= argc)
		throw std::runtime_error ("Too few arguments to --version");
	dfuse::version (std::stoul (argv [first], 0, 16) & 0xffff);
	return 1;
}

int tool::main (int argc, char ** argv) {
	if (argc < 2) {
		help (0, argc, argv);
		return 0;
	}

	for (int i = 1; i < argc; ++i) {
		if (command_list.find (argv [i]) == command_list.end ()) {
			throw std::runtime_error ("Unrecognized command : '" + std::string (argv [i]) + "'");
			continue;
		}
		auto cmdp = command_list [argv [i]];
		i += (this->*cmdp) (i+1, argc, argv);
	}
	return 0;
}



int main (int argc, char ** argv) try {	
	return tool ().main (argc, argv);
}
catch (std::exception const & e) {
	std::cout << e.what () << std::endl;
	return -1;
}