#ifndef __PATCH_H__
#define __PATCH_H__

#include "types.h"
#include <list>
#include <map>

namespace dfu {
	struct patch;

	typedef std::list<patch> patch_list_t;

	struct patch {

		patch (uoff_t iBegin, byte_t const *pData, usize_t iLength);
		patch (uoff_t iBegin, usize_t iEnd, byte_t const *pData);
		patch (uoff_t iBegin, usize_t iEnd, byte_t const iFill);
		patch (std::string const &fileName, uoff_t iBegin = 0);

		patch (patch const &);
		patch (patch &&);
		~patch () ;

		static patch merge (patch_list_t const & ilPatches, byte_t const iGapFill = 0xff);
		static void consolidate (patch_list_t const & pInList, patch_list_t &pOutList, usize_t maxGap = static_cast<usize_t>(-1), byte_t const iGapFill = 0xff);
		static void splitchunks (patch_list_t const & pInList, patch_list_t &pOutList, usize_t maxChunkSize = 30);
		usize_t const &size () const ;
		uoff_t const &base () const;
		uoff_t const &end () const;
		byte_t & operator [] (int);
		byte_t const & operator [] (int) const;
		operator byte_t * ();
		operator byte_t const * () const;
		
		void write (std::string const &fileName) const;
		
	private:
		uoff_t iBegin;
		uoff_t iEnd;	
		usize_t iLength;
		byte_t *pData;
	};

	
}

#endif