#include "srecord.h"
#include "target.h"

using namespace dfu;

target::target (patch_list_t const &_pPatchList, int _iTargetId, std::string const &_pTargetName) :
	pPatchList (_pPatchList),
	iTargetId (_iTargetId),
	pTargetName (_pTargetName)
{}

target::target (target const & t) : 
	pPatchList (t.pPatchList),
	iTargetId (t.iTargetId),
	pTargetName (t.pTargetName)
{}

target::target (target && t) :
	pPatchList (t.pPatchList),
	iTargetId (t.iTargetId),
	pTargetName (t.pTargetName)
{}

target::target (binary_map_t const &_binFileMap, int _iTargetId, std::string const &_pTargetName) :
	pPatchList (),
	iTargetId (_iTargetId),
	pTargetName (_pTargetName)
{
	for (auto const &b: _binFileMap) {
		pPatchList.push_back (patch (b.second, b.first));
	}
}


patch_list_t const & target::patches () const {
	return pPatchList;
}

std::string const &target::name () const {	
	return pTargetName;
}

int target::id () const {
	return iTargetId;
}

target::target (std::string const &_srecFileName, int _iTargetId, std::string const &_pTargetName) :
	pPatchList (),
	iTargetId (_iTargetId),
	pTargetName (_pTargetName)
{	
	dfu::srecord::read (pPatchList, _srecFileName);
}

dword_t target::size () const {
	dword_t _size = 0;
	for (auto const &p: pPatchList) {
		_size += p.size ();
	}
	return _size;
}

