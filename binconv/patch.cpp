#include "patch.h"
#include <algorithm>
#include <stdexcept>
#include <filesystem>

#include <cassert>
#include <cstdio>

using namespace dfu ;

patch::patch (uoff_t _iBegin, byte_t const *_pData, usize_t _iLength) :
	iBegin (_iBegin),
	iEnd (_iBegin + _iLength),
	iLength (_iLength),
	pData (new byte_t [iLength])
{
	if (_pData) {
		std::copy (_pData, _pData + iLength, pData);
	}
	else {
		std::fill (pData, pData + iLength, 0);
	}

}

patch::patch (uoff_t _iBegin, uoff_t _iEnd, byte_t const *_pData = NULL) :
	iBegin (_iBegin),
	iEnd (_iEnd),
	iLength (_iEnd - _iBegin),
	pData (new byte_t [iLength])
{	
	if (_pData) {		
		std::copy (_pData, _pData + iLength, pData);
	}
	else {
		std::fill (pData, pData + iLength, 0);	
	}
}

patch::patch (std::string const &fileName, uoff_t _iBegin) :
	iBegin (_iBegin),
	iEnd (_iBegin + static_cast<uoff_t>(std::tr2::sys::file_size (std::tr2::sys::path (fileName)))),
	iLength (iEnd - iBegin),
	pData (new byte_t [iLength])
{
	if (!std::tr2::sys::exists (std::tr2::sys::path (fileName)) || iLength < 1) {
		delete [] pData;
		throw std::runtime_error ("File "+fileName+" does not exist or is of 0 length");
	}
	std::ifstream (fileName.c_str (), std::ios::binary)
		.read (reinterpret_cast<char *>(pData), iLength);
}

patch::patch (uoff_t _iBegin, uoff_t _iEnd, byte_t const iFill) :
	iBegin (_iBegin),
	iEnd (_iEnd),
	iLength (_iEnd - _iBegin),
	pData (new byte_t [iLength])
{	
	std::fill (pData, pData + iLength, iFill);	
}

patch::patch (patch const & p) :
	iBegin (p.iBegin),
	iEnd (p.iEnd),
	iLength (p.iLength),
	pData (new byte_t [p.iLength])
{
	std::copy (p.pData, p.pData + iLength, pData);
}

patch::patch (patch && p) :
	iBegin (p.iBegin),
	iEnd (p.iEnd),
	iLength (p.iLength),
	pData (p.pData)
{
	p.pData = NULL;
}

patch::~patch () {
	if (pData) {
		delete [] pData;
	}
}

patch patch::merge (std::list<patch> const & patches, byte_t const iGapFill) {
	uoff_t uMinAddr = static_cast<uoff_t> (-1);
	uoff_t uMaxAddr = 0;

	usize_t uTotalSize = 0;
	for (auto const &p: patches) {
		uMinAddr = std::min (uMinAddr, p.iBegin);
		uMaxAddr = std::max (uMaxAddr, p.iEnd);
		uTotalSize += p.iLength;
	}

	usize_t uLength = uMaxAddr - uMinAddr;

	patch pMerged (uMinAddr, uMaxAddr, iGapFill);		
	
	for (auto const &p: patches) {
		uoff_t _iBegin = p.iBegin - uMinAddr;
		uoff_t _iEnd = _iBegin + p.iLength;
		
		std::copy (p.pData, p.pData + p.iLength, pMerged.pData + _iBegin);		
	}

	return pMerged;
}

void patch::consolidate (patch_list_t const &pInList, patch_list_t &pOutList, usize_t maxGap, byte_t gapFill) {
	uoff_t lastEnd = 0;
	patch_list_t mergeList ;

	for (auto const &p: pInList) {
		if (!lastEnd) {
			mergeList.push_back (p);
			lastEnd = p.end ();
			continue;
		}

		if (p.base () - lastEnd > maxGap) {
			pOutList.push_back (merge (mergeList, gapFill));
			mergeList.clear ();			
		}

		mergeList.push_back (p);
		lastEnd = p.end ();
	}

	pOutList.push_back (patch::merge (mergeList, gapFill));
}


usize_t const & patch::size () const {
	return iLength;
}

uoff_t const &patch::base () const{
	return iBegin;
}

uoff_t const &patch::end () const{
	return iEnd;
}

byte_t & patch::operator [] (int i) {
	return pData [i];
}

byte_t const & patch::operator [] (int i) const{
	return pData [i];
}

patch::operator byte_t * () {
	return pData ;
}

patch::operator byte_t const * () const {
	return pData ;
}

void patch::write (std::string const &fileName) const {
	std::ofstream (fileName.c_str (), std::ios::binary)
		.write (reinterpret_cast<char const *>(pData), iLength);
}

void patch::splitchunks (patch_list_t const &pInList, patch_list_t &pOutList, usize_t iMaxChunkSize) {
	for (auto const &p: pInList) {
		if (p.size () <= iMaxChunkSize) {
			pOutList.push_back (p);
			continue;
		}
		usize_t iChunks = (p.size () + iMaxChunkSize - 1) / iMaxChunkSize;
		for (usize_t i = 0; i < iChunks; ++i) {
			pOutList.push_back (patch (
				p.base () + i * iMaxChunkSize, 
				&p [i * iMaxChunkSize], 
				std::min (p.size () - i * iMaxChunkSize, iMaxChunkSize)));
		}
	}
}