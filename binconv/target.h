#ifndef __DFUSE_TARGET_H__
#define __DFUSE_TARGET_H__

#include "patch.h"
#include <list>
#include <utility>
#include <string>

namespace dfu {
	typedef std::pair<dword_t, std::string> binary_map_entry_t;
	typedef std::list<binary_map_entry_t> binary_map_t;

	struct target {
		target (target const & t);
		target (target && t);
		target (patch_list_t const & pList,	int iTargetId = 0, std::string const &pTargetName = "ST...");	// initialize with patch list
		target (std::string const & srecFileName, int iTargetId = 0, std::string const &pTargetName = "ST...");	// initialize from srecord
		target (binary_map_t const & binFileMap, int iTargetId = 0, std::string const &pTargetName = "ST...");	// initialize from binary file map address->bin file

		patch_list_t const & patches () const;
		std::string const & name () const ;
		int id () const;
		dword_t size () const;

	private:
		patch_list_t		pPatchList;
		int					iTargetId;
		std::string			pTargetName;
	};
};

#endif