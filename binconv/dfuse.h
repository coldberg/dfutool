#ifndef __DFUSE_H__
#define __DFUSE_H__

#include "types.h"
#include "patch.h"
#include "target.h"
#include <string>
#include <map>

namespace dfu {

	typedef std::list<target> target_map_t;

	struct dfuse {
		
		dfuse (word_t iFwVersion = 0x0000, word_t iVendorId = 0x0483, word_t iProductId = 0x0000);

		void write_dfuse (std::string const &fileName) const ;
		void write_multibin (std::string const &fileName) const ;
		void write_srecord (std::string const &fileName, usize_t splitSize = 30) const;

		void add_srecord (std::string const &sRecFileName, int iTargetId, std::string const &pTargetName);
		void add_multibin (binary_map_t const &binFileMap, int iTargetId, std::string const &pTargetName);
		void add_dfuse (std::string const &sDfuSeFileName);

		void vendor_id (word_t vid);
		void product_id (word_t pid);
		void version (word_t ver);

		word_t vendor_id () const;
		word_t product_id () const;
		word_t version () const;


	private:


		word_t iFwVersion;
		word_t iVendorId;
		word_t iProductId;
		target_map_t pTargetMap;

		static dword_t crc32 (dword_t crc, void const *buf, size_t size);

		static const byte_t prefix_init [11];
		static const byte_t target_init [274];
		static const byte_t suffix_init [16];
	};
}

#endif