#ifndef __SRECORD_H__
#define __SRECORD_H__

#include <string>
#include <list>
#include <map>
#include "patch.h"

namespace dfu {		
	struct srecord {


		/// private
		static void read (
			patch_list_t &pList, 
			std::string const &fileName);

		static void write (
			patch_list_t const &pList, 
			std::string const &fileName, 
			dword_t iEntryPoint = 0, 
			byte_t const *pHeaderData = NULL, 
			byte_t iHeaderLen = 0, 
			usize_t splitSize = 30);

	private:
		static void srecord::parse (patch_list_t &pList, std::istream &pStream);

		static void		parse_hex_line		(patch_list_t & pList, std::string const &line, int type = 3);
		static usize_t	parse_hex_string	(std::string const & s);
		static byte_t	parse_hex_char		(char const c);

		
		static std::string dword_to_hex	(dword_t data, usize_t bits);

		static std::string srec_line (int type, dword_t uAddress, byte_t iLength, byte_t const *pData);

	};
}



#endif