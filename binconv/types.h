#ifndef __TYPES_H__
#define __TYPES_H__

#include <map>
	
namespace dfu {
	typedef unsigned char		u08_t, ubyte_t, byte_t;
	typedef unsigned short		u16_t, uword_t, word_t;
	typedef unsigned long		u32_t, udword_t, dword_t;
	typedef unsigned long long	u64_t, uqword_t, qword_t;
	typedef signed char			i08_t, ibyte_t;
	typedef signed short		i16_t, iword_t;
	typedef signed long			i32_t, idword_t;
	typedef signed long long	i64_t, iqword_t;
	typedef	int					bool_t;

#ifdef _WIN64
	 typedef i64_t isize_t, ioff_t;
	 typedef u64_t usize_t, uoff_t;
#else
	 typedef i32_t isize_t, ioff_t;
	 typedef u32_t usize_t, uoff_t;
#endif

}

#endif