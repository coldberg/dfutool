#include "srecord.h"
#include <fstream>
#include <stdexcept>
#include <algorithm>

using namespace dfu;


void srecord::read (patch_list_t &pList, std::string const &fileName) {
	auto f = std::ifstream (fileName.c_str ());
	if (!f) {
		throw std::runtime_error ("File not found : '"+fileName+"'");
	}
	srecord::parse (pList, f);	
}


void srecord::parse (patch_list_t &pList, std::istream &pStream) {
	patch_list_t pl;
	std::string srecline;
	int iLineCount = 0;
	while (std::getline (pStream, srecline)) {
		int id = srecline [1] - '0';
		if (id < 0 || id > 9 || srecline [0] != 'S')
			throw std::runtime_error ("Malformed s-record file");
		switch (id) {
			case 0: {
				// header record , discard , not useful
				++iLineCount;
				break;
			};

			case 1:
			case 2:
			case 3: {
				
				parse_hex_line (pl, srecline.substr (2), id);
				++iLineCount;
				break;
			}

			case 4: {
				throw std::runtime_error ("S4 Record is reserved and should not apear in the file.");
				break;
			}


			case 5: 
			case 6: {
				// verify record count
				break;
			}
			
			case 7:
			case 8:
			case 9: {
				// Entry point address , discarded
				break;
			}
			
		}
	}
	patch::consolidate (pl, pList, 0);
}


usize_t srecord::parse_hex_string (std::string const & inhex) {
	usize_t reg;
	if (inhex.length () > sizeof (usize_t)*2)
		throw std::runtime_error ("Input hex string too long : '" + inhex + "'");
	for (auto const &c : inhex) {
		reg = (reg << 4) | parse_hex_char (c);
	}
	return reg;
}

byte_t srecord::parse_hex_char (char const c) {
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'a' && c <= 'f')
		return 10 + c - 'a';
	if (c >= 'A' && c <= 'F')
		return 10 + c - 'A';
	throw std::runtime_error (std::string ("Invalid char in hex string '") + c + std::string ("'"));
	return 0;
}

void srecord::parse_hex_line (patch_list_t &pList, std::string const &pLine, int type) {	
	byte_t buffer [256];
	usize_t addrl = 1+type;
	byte_t len = parse_hex_string (pLine.substr (0, 2)) & 0xff;
	uoff_t addr = parse_hex_string (pLine.substr (2, addrl*2));
	std::string data = pLine.substr (2 + addrl*2);
	usize_t csum = len + (addr & 0xff) + ((addr >> 8) & 0xff);	
	if (type > 1) csum += (addr >> 16) & 0xff;
	if (type > 2) csum += (addr >> 24) & 0xff;
	for (uoff_t i = 0;i < len - addrl; ++i) {
		buffer [i] = parse_hex_string (data.substr (i*2, 2)) & 0xff; 
		if (i < len - addrl - 1) {
			csum += buffer [i];
			continue ;
		}				
		if (((~csum) & 0xff) == buffer [i])
			continue;
		throw std::runtime_error ("Failed checksum");		
	}

	pList.push_back (dfu::patch (addr, buffer, len-addrl-1));
}


void srecord::write (patch_list_t const &pList, std::string const &fileName, dword_t iEntryPoint, byte_t const *pHeaderData, byte_t iHeaderLen, usize_t splitSize) {
	std::ofstream srec (fileName.c_str ());
	patch_list_t pSplitList;

	splitSize = std::min (static_cast<usize_t> (250), splitSize);
	patch::splitchunks (pList, pSplitList, splitSize);

	if (!pHeaderData || !iHeaderLen) {
		pHeaderData = reinterpret_cast<byte_t const *>("HDR");
		iHeaderLen = 3;
	}

	srec << srec_line (0, 0x0000, iHeaderLen, pHeaderData) << std::endl;

	for (auto const &p: pSplitList) {		
		srec << srec_line (3, p.base (), p.size () & 0xff, &p [0]) << std::endl;
	}

	srec << srec_line (8, iEntryPoint, 0, NULL);

}

std::string srecord::dword_to_hex (dword_t data, usize_t bits) {
	static const char hex_char [] = "0123456789ABCDEF";
	std::string hexstr = "";

	if (bits > sizeof (data)*8)
		throw std::runtime_error ("Too many bits.");

	data = data << (sizeof (data)*8 - bits);

	for (usize_t i = 0; i < (bits >> 2); ++i) {
		hexstr += hex_char [(data >> (sizeof (data)*8 - 4)) & 0xf];
		data = data << 4;
	}
	return hexstr;	
}

std::string srecord::srec_line (int iType, dword_t iAddress, byte_t iLength, byte_t const *pData) {	
	static byte_t const iAddrLenByType [] = {
		2,	//0
		2,	//1
		3,	//2
		4,	//3
		0,	//4 reserved
		2,	//5
		3,	//6
		4,  //7
		3,  //8
		2,  //9
	};


	if (iType < 0 || iType > 9) {
		throw std::runtime_error ("Unknown record type when writting s-record");
	}

	
	std::string line ("S" + dword_to_hex (iType, 4));

	byte_t len = iLength + iAddrLenByType [iType] + 1 ;

	byte_t csum = len + (iAddress & 0xff) + ((iAddress >> 8) & 0xff) ;
	if (iAddrLenByType [iType] > 2)
		csum += (iAddress >> 16) & 0xff;
	if (iAddrLenByType [iType] > 3)
		csum += (iAddress >> 24) & 0xff;
	
	line += dword_to_hex (len, sizeof (len) * 8);
	line += dword_to_hex (iAddress, iAddrLenByType [iType] * 8);
	for (int i = 0; pData && i < iLength; ++i) {
		line += dword_to_hex (pData [i], 8);
		csum += pData [i];
	}
	csum = (~csum) & 0xff;
	line += dword_to_hex (csum, 8);
	return line;
}